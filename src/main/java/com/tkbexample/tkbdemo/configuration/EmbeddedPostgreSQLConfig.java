package com.tkbexample.tkbdemo.configuration;


import de.flapdoodle.embed.process.io.directories.FixedPath;
import de.flapdoodle.embed.process.runtime.Network;
import de.flapdoodle.embed.process.store.PostgresArtifactStoreBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.yandex.qatools.embed.postgresql.*;
import ru.yandex.qatools.embed.postgresql.config.AbstractPostgresConfig;
import ru.yandex.qatools.embed.postgresql.config.PostgresConfig;
import ru.yandex.qatools.embed.postgresql.config.PostgresDownloadConfigBuilder;
import ru.yandex.qatools.embed.postgresql.config.RuntimeConfigBuilder;
import ru.yandex.qatools.embed.postgresql.distribution.Version;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static ru.yandex.qatools.embed.postgresql.distribution.Version.Main.V9_6;

@Configuration
@EnableTransactionManagement
public class EmbeddedPostgreSQLConfig {

    @Value("${db.host}")
    private String host;

    @Value("${db.port}")
    private int port;

    @Value("${db.name}")
    private String name;

    @Value("${db.username}")
    private String username;

    @Value("${db.password}")
    private String password;



    @Bean
    public EmbeddedPostgres embeddedPostgreSQLProcess() throws IOException {
        final EmbeddedPostgres postgres = new EmbeddedPostgres(V9_6);
        final String url = postgres.start(host, port, name, username, password);
        return postgres;
    }

    @Bean
    @DependsOn("embeddedPostgreSQLProcess")
    public DataSource dataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.postgresql.Driver");
        ds.setUrl(String.format("jdbc:postgresql://%s:%s/%s", "localhost", 5432, "dbName"));
        ds.setUsername("userName");
        ds.setPassword("password");
        return ds;
    }

}
