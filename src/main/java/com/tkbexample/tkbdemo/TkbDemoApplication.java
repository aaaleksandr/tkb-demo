package com.tkbexample.tkbdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TkbDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TkbDemoApplication.class, args);
	}

}
