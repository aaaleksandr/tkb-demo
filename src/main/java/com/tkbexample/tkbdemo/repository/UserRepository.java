package com.tkbexample.tkbdemo.repository;

import com.tkbexample.tkbdemo.entity.UserDB;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserDB, Long> {
    Optional<UserDB> findByLogin(String login);
}
