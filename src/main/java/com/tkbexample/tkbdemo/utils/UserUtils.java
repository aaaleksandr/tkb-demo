package com.tkbexample.tkbdemo.utils;

import com.tkbexample.tkbdemo.entity.UserDB;
import io.tkb_demo.demo.User;
import org.springframework.stereotype.Service;

@Service
public class UserUtils {

    public User fromUserDbToUser(UserDB userDB){
        if (userDB == null) return null;
        User user = new User();
        user.setId(userDB.getId());
        user.setFirstName(userDB.getFirstName());
        user.setLastName(userDB.getLastName());
        user.setLogin(userDB.getLogin());
        return user;
    }

}
