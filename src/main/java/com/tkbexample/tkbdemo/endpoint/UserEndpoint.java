package com.tkbexample.tkbdemo.endpoint;

import com.tkbexample.tkbdemo.service.UserService;
import io.tkb_demo.demo.UserGetRequest;
import io.tkb_demo.demo.UserResponse;
import io.tkb_demo.demo.UserSaveRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


@Endpoint
public class UserEndpoint {

    private UserService userService;

    @Autowired
    public UserEndpoint(UserService userService){
        this.userService = userService;
    }


    @PayloadRoot(namespace = "http://tkb-demo.io/demo", localPart = "UserGetRequest")
    @ResponsePayload
    public UserResponse getUser(@RequestPayload UserGetRequest request) {
        return userService.getUser(request);
    }

    @PayloadRoot(namespace = "http://tkb-demo.io/demo", localPart = "UserSaveRequest")
    @ResponsePayload
    public UserResponse saveUser(@RequestPayload UserSaveRequest request) {
        return userService.saveUser(request);
    }

}
