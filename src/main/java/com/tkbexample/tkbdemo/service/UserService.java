package com.tkbexample.tkbdemo.service;

import io.tkb_demo.demo.UserGetRequest;
import io.tkb_demo.demo.UserResponse;
import io.tkb_demo.demo.UserSaveRequest;

public interface UserService {
    UserResponse getUser(UserGetRequest request);

    UserResponse saveUser(UserSaveRequest request);
}
