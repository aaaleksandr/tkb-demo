package com.tkbexample.tkbdemo.service.impl;

import com.tkbexample.tkbdemo.entity.UserDB;
import com.tkbexample.tkbdemo.repository.UserRepository;
import com.tkbexample.tkbdemo.service.UserService;
import com.tkbexample.tkbdemo.utils.UserUtils;
import io.tkb_demo.demo.User;
import io.tkb_demo.demo.UserGetRequest;
import io.tkb_demo.demo.UserResponse;
import io.tkb_demo.demo.UserSaveRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private UserUtils userUtils;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                        UserUtils userUtils){
        this.userRepository = userRepository;
        this.userUtils = userUtils;
    }

    @Override
    public UserResponse getUser(UserGetRequest request){
        Optional<UserDB> optionalUserDB = userRepository.findByLogin(request.getLogin());
        UserDB userDB = optionalUserDB
                .orElseThrow(() -> new ExpressionException(String.format("User with login %s not found", request.getLogin())));
        User user = userUtils.fromUserDbToUser(userDB);
        UserResponse userResponse = new UserResponse();
        userResponse.setUser(user);
        return userResponse;
    }

    @Override
    public UserResponse saveUser(UserSaveRequest request){
        UserDB.UserDBBuilder builder = UserDB.builder();
        builder.firstName(request.getFirstName());
        builder.lastName(request.getLastName());
        builder.login(request.getLogin());
        UserDB userDB = builder.build();
        userRepository.save(userDB);
        User user = userUtils.fromUserDbToUser(userDB);
        UserResponse userResponse = new UserResponse();
        userResponse.setUser(user);
        return userResponse;
    }

}
